import React from 'react'
import {formatPrice} from '../helpers'

class Order extends React.Component{

    renderOrder = key => {
        const fish = this.props.fishes[key]
        const count = this.props.orders[key]
        const isAvailable = fish && fish.status === 'available'

        if(!fish) return null

        if(!isAvailable)
            return <li key={key}>Sorry {fish ? fish.name : 'fish'} is no longer available</li>
        else
            return (
                <li key={key}>
                    {count} lbs {fish.name}
                    {formatPrice(fish.price * count)}
                </li>
            )
    }

    render(){
        const ordersId = Object.keys(this.props.orders)
        const total = ordersId.reduce((prevTotal, key) => {
            const fish = this.props.fishes[key]
            const count = this.props.orders[key]
            const isAvailable = fish && fish.status === 'available'
            if(isAvailable)
                return prevTotal + (count * fish.price)
            else
                return prevTotal

        }, 0)

        return (
            <div className="order-wrap">
                <h2>Order</h2>
                <ul className="order">
                    { ordersId.map(this.renderOrder)}
                </ul>
                <div className="total">
        Total <strong>{formatPrice(total)}</strong>
                </div>
            </div>
        )
    }
}

export default Order