import React from 'react';
import Header from './Header'
import Inventory from './Inventory'
import Order from './Order'
import sampleFishes from '../sample-fishes'
import Fish from './Fish'
import base from '../base'

class App extends React.Component{
    state = {
        fishes: {},
        orders: {}
    }

    componentDidMount(){
        const {params} = this.props.match
        const localStorageRef = localStorage.getItem(params.storeId)

        if(localStorageRef)
            this.setState({orders: JSON.parse(localStorageRef)})

        this.ref = base.syncState(`${params.storeId}/fishes`, {
            context: this,
            state: 'fishes'
        })
    }

    componentDidUpdate(){
        localStorage.setItem(
            this.props.match.params.storeId,
            JSON.stringify(this.state.orders)
        )
    }

    componentWillUnmount(){
        base.removeBinding(this.ref)
    }

    addFish = fish => {
        const fishes = {...this.state.fishes}
        fishes[`fish${Date.now()}`] = fish
        this.setState({fishes})
    }

    updateFish = (key, updatedFish) => {
        const fishes = {...this.state.fishes}
        fishes[key] = updatedFish
        this.setState({fishes})
    }

    addToOrder = key => {
        const orders = {...this.state.orders}
        orders[key] = orders[key] + 1 || 1
        this.setState({orders})
    }

    loadSampleFishes = () => {
        this.setState({fishes: sampleFishes})
    }

    render(){
        console.log('App renders',JSON.stringify(this.state))
        return (      
        <div className="catch-of-the-day">
            <div className="menu">
                <Header tagline="Fresh daily"/>
                <ul className="fishes">
                    { Object.keys(this.state.fishes).map(key => 
                    <Fish 
                    key={key} 
                    index={key}
                    details={this.state.fishes[key]}
                    addToOrder={this.addToOrder}
                    /> )
                    }
                </ul>
            </div>
            <Order orders={this.state.orders} fishes={this.state.fishes}/>
            <Inventory addFish={this.addFish} loadSampleFishes={this.loadSampleFishes} fishes={this.state.fishes} updateFish={this.updateFish}/>
        </div>
        )
    }
}

export default App